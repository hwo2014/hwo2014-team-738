import json

data_json = open('./race3.orig.json')
data = json.load(data_json)

for d in data:
  if 'msgType' in d and d['msgType'] == 'fullCarPositions' and 'gameTick' in d:
    print d['gameTick'], d['data'][0]['angle'], d['data'][0]['angleOffset'], d['data'][0]['piecePosition']['inPieceDistance']
